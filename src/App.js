import React from 'react'
import { Switch, Route } from "react-router-dom";
export const App = () => {
  return (
    <div
      style={{
        fontFamily: "sans-serif",
        display: "flex",
        minHeight: "100vh",
        alignItems: "center",
        justifyContent: "center",
        fontSize: 22,
      }}
    >
      <Switch>
        <Route path="/result/error_link">Ошибка ссылки</Route>
        <Route path="/result/completed">Данный опрос Вами пройден</Route>
        <Route path="/result/survey_closed">Опрос закрыт</Route>
        <Route>Страница не найдена</Route>
      </Switch>
    </div>
  );
};
